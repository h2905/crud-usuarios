import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Users } from '../models/usuario';

@Injectable({
  providedIn: 'root',
})
export class UsuarioService {
  url = 'http://localhost:8095/getUsers/';
  saveurl = 'http://localhost:8095/saveUser';
  updateurl = 'http://localhost:8095/updateUser/';
  deleteurl = 'http://localhost:8095/deleteUser/';
  constructor(private http: HttpClient) {}

  getUsuarios(): Observable<any> {
    return this.http.get(this.url);
  }
  guardarUsuario(usuario: Users): Observable<any> {
    return this.http.post(this.saveurl, usuario);
  }

  obtenerUsuario(id: string): Observable<any> {
    return this.http.get(this.url + id);
  }

  editarUsuario(id: String, usuario: Users): Observable<any> {
    return this.http.put(this.updateurl + id, usuario);
  }

  eliminarUsuario(id: string): Observable<any> {
    return this.http.delete(this.deleteurl + id);
  }
}

export class Users {
  id?: any;
  user?: string;
  name?: string;
  lastName?: string;
  email?: string;
  dni?: string;
  age?: string;
  createUserDate?: string;
  born?: string;

  constructor(
    user: string,
    lastName: string,
    email: string,
    dni: string,
    age: string,
    createUserDate: string,
    born:string
  ) {
    this.user = user;
    this.lastName = lastName;
    this.email = email;
    this.dni = dni;
    this.age = age;
    this.createUserDate = createUserDate;
    this.born = born;
  }
}

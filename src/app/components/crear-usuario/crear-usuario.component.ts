import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Users } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css'],
})
export class CrearUsuarioComponent implements OnInit {
  usuarioForm: FormGroup;
  titulo = 'Crear usuario';
  id: string | null;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private _usuarioService: UsuarioService,
    private aRouter: ActivatedRoute
  ) {
    this.usuarioForm = this.fb.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      user: ['', Validators.required],
      dni: ['', Validators.required],
    });
    this.id = this.aRouter.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.esEditar();
  }

  agregarProducto() {
    const USUARIO: Users = {
      name: this.usuarioForm.get('name')?.value,
      lastName: this.usuarioForm.get('lastName')?.value,
      email: this.usuarioForm.get('email')?.value,
      dni: this.usuarioForm.get('dni')?.value,
      user: this.usuarioForm.get('user')?.value,
    };

    if (this.id !== null) {
      console.log(USUARIO);
      this._usuarioService.editarUsuario(this.id, USUARIO).subscribe(
        (data) => {
          this.toastr.info(
            'El usuario fue actualizado con exito!',
            'Usuario Actualizado!'
          );
          this.router.navigate(['/']);
        },
        (error) => {
          console.log(error);
          this.usuarioForm.reset();
        }
      );
    } else {
      console.log(USUARIO);
      this._usuarioService.guardarUsuario(USUARIO).subscribe(
        (data) => {
          this.toastr.success(
            'El usuario fue registrado con exito!',
            'Usuario Registrado!'
          );
          this.router.navigate(['/']);
        },
        (error) => {
          console.log(error);
          this.usuarioForm.reset();
        }
      );
    }
  }

  esEditar() {
    if (this.id !== null) {
      this.titulo = 'Editar usuario';
      this._usuarioService.obtenerUsuario(this.id).subscribe((data) => {
        this.usuarioForm.setValue({
          name: data.name,
          lastName: data.lastName,
          email: data.email,
          user: data.user,
          dni: data.dni,
        });
      });
    }
  }
}
